package org.bitbucket.mineguild.MineguildAdmin;

import org.bukkit.command.CommandExecutor;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.alecgorge.minecraft.jsonapi.JSONAPI;
import com.alecgorge.minecraft.jsonapi.api.APIMethodName;
import com.alecgorge.minecraft.jsonapi.api.JSONAPICallHandler;
import com.avaje.ebean.enhance.agent.MethodNewInstance;

public class Main extends JavaPlugin implements JSONAPICallHandler {
	@Override
	public void onDisable() {
		System.out.println("MineguildAdmin Ver. " + this.getDescription().getVersion() + "wurde deaktiviert!");
		this.saveDefaultConfig();
		this.saveConfig();
	}
	@Override
	public void onEnable() {
		System.out.println("MineguildAdmin Ver. " + this.getDescription().getVersion() + " wurde aktiviert!");
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();
		
		Plugin checkplugin = this.getServer().getPluginManager().getPlugin("JSONAPI");
		if(checkplugin != null){
			JSONAPI jsonapi = (JSONAPI)checkplugin;
			
			jsonapi.registerAPICallHandler(this);
		}
		else{
			System.out.println("Das Plugin kann nicht vollständig funktionieren weil JSONAPI fehlt");
		}
		
		CommandExecutor MGACE = new MGACommandExecutor(this);
		
		
		getCommand("setspawn").setExecutor(MGACE);
		getCommand("gm").setExecutor(MGACE);
		getCommand("spawn").setExecutor(MGACE);
		
	}
	
	//Variablen
	String playerOnly = "Du kannst dieses Kommando nur als Spieler benutzen";

	@Override
	public Object handle(APIMethodName methodName, Object[] args) {
		if(methodName.matches("ping")){
			return "pong";
		}
		return false;
	}
	@Override
	public boolean willHandle(APIMethodName methodName) {
		if(methodName.matches("ping")){
			return true;
		}
		return false;
	}
}
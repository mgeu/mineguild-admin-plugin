package org.bitbucket.mineguild.MineguildAdmin.Commands;

import org.bitbucket.mineguild.MineguildAdmin.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class CommandSPAWN {
	
	Command cmd;
	CommandSender sender;
	Player t;
	String[] args;
	Main plugin;
	Vector v;
	String world;
	float pi;
	float y;
	
	public CommandSPAWN(Command cmd, String[] args, CommandSender sender, Main plugin){
		this.cmd = cmd;
		this.sender = sender;
		this.args = args;
		this.plugin = plugin;
	}
	
	public boolean execute(){
		t = (Player) sender;
		world = t.getWorld().toString();
		v = plugin.getConfig().getVector("Config.spawn." + world + ".v");
		pi = (float) plugin.getConfig().getDouble("Config.spawn." + world + ".pi");
		y = (float) plugin.getConfig().getDouble("Config.spawn." + world + ".y");
		if(v != null){
			t.teleport(v.toLocation(t.getWorld(), y, pi));
			return true;
		}
		else if(t.hasPermission("mga.setspawn")){
			sender.sendMessage("Der Vanilla-Spawn wurde jetzt zum Welt Plugin-Spawn gesetzt.");
			
			v = t.getWorld().getSpawnLocation().add(0, 10, 0).toVector();
			
			plugin.getConfig().set("Config.spawn." + world + ".v", v);
			plugin.getConfig().set("Config.spawn." + world + ".pi", 0);
			plugin.getConfig().set("Config.spawn." + world + ".y", 0);
			return true;
		}
		else if(!(t.hasPermission("mga.setspawn"))){
			t.teleport(t.getWorld().getSpawnLocation().add(0, 10, 0));
			return true;
		}
		return false;
	}

}

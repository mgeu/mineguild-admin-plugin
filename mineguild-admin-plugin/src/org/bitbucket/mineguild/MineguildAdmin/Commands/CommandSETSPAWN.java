package org.bitbucket.mineguild.MineguildAdmin.Commands;

import org.bitbucket.mineguild.MineguildAdmin.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;


public class CommandSETSPAWN {
	
	Command cmd;
	CommandSender sender;
	Player p;
	String[] args;
	String cpath;
	Vector v;
	double pi;
	double y;
	Main plugin;
	String world;
	

	
	public CommandSETSPAWN(Command cmd, String[] args, CommandSender sender, Main plugin){
		this.cmd = cmd;
		this.args = args;
		this.sender = sender;
		this.cpath = "Config.spawn.";
		this.plugin = plugin;
	}
	
	public boolean execute(){
		p = (Player) sender;
		world = p.getWorld().toString();
		v = p.getLocation().toVector();
		pi = p.getLocation().getYaw();
		y =  p.getLocation().getYaw();
		plugin.getConfig().set("Config.spawn." + world + ".v", v);
		plugin.getConfig().set("Config.spawn." + world + ".pi", pi);
		plugin.getConfig().set("Config.spawn." + world + ".y", y);
		plugin.saveConfig();
		return true;
	}

}


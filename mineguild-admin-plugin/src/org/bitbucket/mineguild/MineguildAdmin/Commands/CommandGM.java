package org.bitbucket.mineguild.MineguildAdmin.Commands;

import org.bitbucket.mineguild.MineguildAdmin.Main;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class CommandGM {
	
	Command cmd;
	CommandSender sender;
	String[] args;
	Player target;
	Main plugin;
	
	
	public CommandGM(Command cmd, String[] args, CommandSender sender, Main plugin){
		this.cmd = cmd;
		this.args = args;
		this.sender = sender;
		this.plugin = plugin;
	}

	public boolean execute(){
		
		if(args.length == 0){
			target = (Player) sender;
			}
		else if(args.length == 1){
				
			try{
				target = (Player) sender.getServer().getPlayer(args[0]);
				if(!target.isOnline()){
					throw new NullPointerException();
				}
			} catch(NullPointerException e){
				sender.sendMessage("Spieler ist nicht online oder existiert nicht!");
				return false;
			}
		}
			
		if(target.getGameMode() == GameMode.SURVIVAL)
			target.setGameMode(GameMode.CREATIVE);
		else if(target.getGameMode() == GameMode.CREATIVE)
			target.setGameMode(GameMode.SURVIVAL);
		return true;
	}
}

package org.bitbucket.mineguild.MineguildAdmin;

import org.bitbucket.mineguild.MineguildAdmin.CCommandsGM.CCommandGM;
import org.bitbucket.mineguild.MineguildAdmin.Commands.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class MGACommandExecutor implements CommandExecutor {
	
	private Main plugin;
	
	public MGACommandExecutor(Main plugin){
		this.plugin = plugin;
	}
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		
		
		if(cmd.getName().equalsIgnoreCase("gm")){
			if(sender instanceof Player){
				return new CommandGM(cmd, args, sender, this.plugin).execute();
			}
			else if(!(sender instanceof Player)){
				return new CCommandGM(sender, cmd, args).execute();
			}
		}
		
		
		
		if(cmd.getName().equalsIgnoreCase("setspawn") && sender instanceof Player){
			if(sender instanceof Player)
				return new CommandSETSPAWN(cmd, args, sender, this.plugin).execute();
			else if(!(sender instanceof Player))
				sender.sendMessage(plugin.playerOnly);
		}
		
		
		if(cmd.getName().equalsIgnoreCase("spawn")){
			if(sender instanceof Player){
				return new CommandSPAWN(cmd, args, sender, this.plugin).execute(); 
			}
			else if(!(sender instanceof Player)){
				return false;
			}
		}
		
		
		
		return false;
	}

}